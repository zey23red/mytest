#Installs apache2
package 'apache2' do
  action :install
end

service 'apache2' do
  # declare actions the service supports
  supports status: true, restart: true, reload: true
  action [ :enable, :start ]
end